# CQRS and Event-based microservices for accommodations and review scores

## What is under the hood?

- Simple [microservices](https://microservices.io/) implementation
- Service to service communication with [Domain events](https://microservices.io/patterns/data/domain-event.html)
- Services implemented with [CQRS](https://martinfowler.com/bliki/CQRS.html) and inpired by [Uncle Bob's Clean architecture](Clean_Architecture) with [Event sourcing](https://eventstore.com/blog/what-is-event-sourcing/)

    ![](https://blob.jacobsdata.com/software-alchemy/entry8/clean-domain-driven-design-jacobs-510.png)

## Why build it like that?

- With microservices, scalability is made easy compare to a monolith system.
- With microservices, maintainability is easy also, services are small, it's easy to be maintained and to be replaced in a short time
- With CQRS, Domain Events, you have asynchronous system, according to [CAP therom](https://en.wikipedia.org/wiki/CAP_theorem) you cannot achieve consistency, availability, and partition tolerance at the same time, with asynchronous system , you are going to achieve eventually consistent system.

## How do we?

### How to run the system?

- Prerequesits: install [docker](https://www.docker.com/)
- For the first time, you need to set it up

    ```bash
    make setup
    ```

- To run it:

    ```bash
    make up
    ```

- To stop it:

    ```bash
    make down
    ```


### How do we use the data import tool?

- For Accommodations

    ```bash
    curl --location --request PATCH '127.0.0.1:3001/accommodations' \
    --header 'Content-Type: application/json' \
    --data-binary '@/[ACCOMODATIONS_FILE_DIRECTORY]/accommodations.json'
    ```

- For Reviews

    ```bash
    curl --location --request PATCH '127.0.0.1:3001/reviews' \
    --header 'Content-Type: application/json' \
    --data-binary '@/[REVIEWS_FILE_DIRECTORY]/reviews.json'
    ```

### How do we use accommodations web service endpoints?

- List all of accommodations: [127.0.0.1:3001/accommodations](127.0.0.1:3001/accommodations)
- Get single accommodation: [127.0.0.1:3001/accommodations/UUID](127.0.0.1:3001/accommodations/uuid)
- List all reviews for an accommodation: [127.0.0.1:3001/accommodations/UUID/reviews](127.0.0.1:3001/accommodations/uuid/reviews)
- Get a single review for an accommodation: [127.0.0.1:3001/accommodations/UUID/reviews/UUID](127.0.0.1:3001/accommodations/uuid/reviews/uuid)

### How do we use scores web service endpoints?

- Get accommodation general rating score: [127.0.0.1:3002/accommodations/UUID/general-score](127.0.0.1:3002/accommodations/UUID/genral-score)
- Get accommodation aspects rating score: [127.0.0.1:3002/accommodations/UUID/aspect-scores](127.0.0.1:3002/accommodations/UUID/aspect-scores)

## Descoped things:
*Why?* It will take extra time or efforts with low impact, but if it's really needed, I will be happy to do it with extra time.

- Console command for the import tool, I used RESTful PATCH request to achive it, and it's not optimized
- Proper input validation and exception mapping to correct http status
- Proper Dtos and transformers
- Proper table designs for relational DB
- I'm not fluent in Python, so I didn't write accommodations web service in Python.
- Tests
- Auth
- Logs

## Nice to have:
- Using TypeScript with [Nest.js](https://docs.nestjs.com/)
- Health check
- Reverse proxy for dev
- Migration tool
- CI/CD
- Infrastructure code to provision it
- Frontend client
