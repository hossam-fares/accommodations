CREATE TABLE accommodation
(
    id UUID NOT NULL PRIMARY KEY,
    vrwId INTEGER,
    names JSON,
    type VARCHAR(11),
    media JSON,
    geo JSON,
    stars INTEGER,
    addres JSON,
    contactInformation JSON,
    awards JSON,
    facilities JSON,
    heliosUrl VARCHAR(256),
    heliosHistoricalUrls JSON,
    bookingComBlockEnabled boolean,
    premiumAdlinkEnabled boolean,
    owner JSON,
    status JSON,
    parents JSON,
    heliosId INTEGER,
    updatedDate TIMESTAMP,
    popularityScore NUMERIC(5,2)
);

CREATE TABLE review
(
    id UUID NOT NULL PRIMARY KEY,
    userData JSON,
    titles JSON,
    texts JSON,
    travelDate TIMESTAMP,
    locale VARCHAR(64),
    ratings JSON,
    traveledWith VARCHAR(64),
    cooperation JSON,
    entryDate TIMESTAMP,
    originalUserName VARCHAR(64),
    originalUserEmail VARCHAR(100),
    originalUserIp VARCHAR(64),
    status JSON,
    parents JSON,
    heliosId INTEGER,
    updatedDate TIMESTAMP
);
