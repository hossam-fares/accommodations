const util = require('util')
const RedisSMQ = require('rsmq')
const rsmq = new RedisSMQ({ host: process.env.QUEUE_HOST, port: 6379, ns: 'rsmq' })

const createQueue = util.promisify(rsmq.createQueue)
const listQueues = util.promisify(rsmq.listQueues)
const receiveMessage = util.promisify(rsmq.receiveMessage)
const deleteMessage = util.promisify(rsmq.deleteMessage)
const qname = 'accommodation_events'

async function receiveOnce() {
    const queues = await listQueues()

    if (!queues.includes(qname)) {
        await createQueue({ qname })
    }

    return await receiveMessage({ qname })
}

async function acknowledgeMessage(id) {
    await deleteMessage({ qname, id })
}

module.exports = {
    receiveOnce,
    acknowledgeMessage
}
