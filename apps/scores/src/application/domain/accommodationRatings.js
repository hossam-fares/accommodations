
class AccommodationRatings {
    constructor(
        reviewId,
        accommodationId,
        travelDate,
        generalRatings,
        aspectRatings
    ) {
        this.reviewId = reviewId
        this.accommodationId = accommodationId
        this.travelDate = travelDate
        this.generalRatings = generalRatings
        this.aspectRatings = aspectRatings
    }
}

module.exports = AccommodationRatings
