
class accommodationRatingScores {
    constructor(
        accommodationId,
        generalRatingScore,
        aspectRatingScores
    ) {
        this.accommodationId = accommodationId
        this.generalRatingScore = generalRatingScore
        this.aspectRatingScores = aspectRatingScores
    }
}

module.exports = accommodationRatingScores
