const accommodationRatingsRepository = require('../../persistence/repositories/accommodationRatingsRepository')
const accommodationRatingScoresRepository = require('../../persistence/repositories/accommodationRatingScoresRepository')
const scoresCalculator = require('../services/scoresCalculator')

async function handel(upsertAccommodationRatingsCommand) {
    const accommodationRatings = upsertAccommodationRatingsCommand.accommodationRatings
    const accommodationId = accommodationRatings.accommodationId
    await accommodationRatingsRepository.upsert(accommodationRatings)
    const accommodationRatingsCollection = await accommodationRatingsRepository.getAll(accommodationId)
    const accommodationRatingScores = scoresCalculator.calculate(accommodationId, accommodationRatingsCollection)
    await accommodationRatingScoresRepository.upsert(accommodationRatingScores)
}

module.exports = { handel }
