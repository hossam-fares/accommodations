const accommodationRatingScoresRepository = require('../../persistence/repositories/accommodationRatingScoresRepository')

async function handel(getAccommodationRatingScores) {
    return accommodationRatingScoresRepository.get(getAccommodationRatingScores.accommodationId)
}

module.exports = { handel }
