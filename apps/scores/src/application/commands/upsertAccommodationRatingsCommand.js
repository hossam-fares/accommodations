class UpsertAccommodationRatingsCommand {
    constructor(accommodationRatings) {
        this.name = 'upsertAccommodationRatings'
        this.accommodationRatings = accommodationRatings
    }
}

module.exports = UpsertAccommodationRatingsCommand
