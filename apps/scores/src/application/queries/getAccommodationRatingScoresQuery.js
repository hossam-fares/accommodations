class GetAccommodationRatingScoresQuery {
    constructor(accommodationId) {
        this.name = 'getAccommodationRatingScores'
        this.accommodationId = accommodationId
    }
}

module.exports = GetAccommodationRatingScoresQuery
