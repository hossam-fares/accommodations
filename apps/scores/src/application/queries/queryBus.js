const queryHandllerMapping = {
    'getAccommodationRatingScores': require("../handlers/getAccommodationRatingScoresHandler")
}

module.exports = {
    dispatch: (query) => {
        const handler = queryHandllerMapping[query.name];

        if (!handler) {
            return Promise.reject({ 'message': 'QueryBus unable to handle ' + query.name });
        }

        return handler.handel(query)
    }
}
