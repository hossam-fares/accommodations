const AccommodationRatingScores = require('../domain/accommodationRatingScores')

function calculateReviewWeight(ts) {
    const monthDiff = monthsDiff(new Date(ts))
    let scale = 1.77

    if (monthDiff < 24) {
        scale = 25 - monthDiff
    }

    return Math.log(scale)
}

function monthsDiff(date) {
    const now = new Date()
    const yearDiff = now.getFullYear() - date.getFullYear()
    let months = (yearDiff * 12) + now.getMonth() - date.getMonth()

    return months;
}

function calculateGeneralScore(accommodationRatingsCollection) {
    let totalReviewWeight = 0
    let totalWeightedRate = 0

    for (const accommodationRatings of accommodationRatingsCollection) {
        if (!accommodationRatings.generalRatings) {
            continue
        }

        const reviewWeight = calculateReviewWeight(accommodationRatings.travelDate)
        const weightedRate = reviewWeight * accommodationRatings.generalRatings
        totalReviewWeight += reviewWeight
        totalWeightedRate += weightedRate
    }

    return parseInt(totalWeightedRate / totalReviewWeight)
}


function calculateAspectScores(accommodationRatingsCollection) {
    const aspectScoreWeights = {}
    const aspectScores = {}

    for (const accommodationRatings of accommodationRatingsCollection) {
        const reviewWeight = calculateReviewWeight(accommodationRatings.travelDate)

        for (const aspectName in accommodationRatings.aspectRatings) {
            const aspectRate = accommodationRatings.aspectRatings[aspectName]

            if (!aspectRate) {
                aspectScoreWeights[aspectName] = null
                continue
            }

            const weightedAspectRate = aspectRate * reviewWeight

            if (aspectScoreWeights[aspectName]) {
                aspectScoreWeights[aspectName].totalReviewWeight += reviewWeight
                aspectScoreWeights[aspectName].totalWeightedRate += weightedAspectRate
            } else {
                aspectScoreWeights[aspectName] = {
                    totalReviewWeight: reviewWeight,
                    totalWeightedRate: weightedAspectRate
                }
            }
        }

        for (const aspectName in aspectScoreWeights) {
            if (!aspectScoreWeights[aspectName]) {
                aspectScores[aspectName] = null
                continue
            }
            aspectScores[aspectName] = parseInt(
                aspectScoreWeights[aspectName].totalWeightedRate / aspectScoreWeights[aspectName].totalReviewWeight
            )
        }
    }

    return aspectScores
}

module.exports = {
    calculate(accommodationId, accommodationRatingsCollection) {
        return new AccommodationRatingScores(
            accommodationId,
            calculateGeneralScore(accommodationRatingsCollection),
            calculateAspectScores(accommodationRatingsCollection)
        )
    }
}
