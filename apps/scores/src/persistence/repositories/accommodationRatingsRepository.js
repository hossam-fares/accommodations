const client = require('../redisClient')

module.exports = {
    upsert: (accommodationRatings) => {
        return client.hset(
            `accommodation_rating_${accommodationRatings.accommodationId}`,
            [
                accommodationRatings.reviewId,
                JSON.stringify(accommodationRatings)
            ]
        )
    },
    getAll: async (accommodationId) => {
        const results = await client.hgetall(`accommodation_rating_${accommodationId}`)
        const accommodationRatingsCollection = []

        for (const reviewId in results) {
          accommodationRatingsCollection.push(JSON.parse(results[reviewId]))
        }

        return accommodationRatingsCollection;
    }
}
