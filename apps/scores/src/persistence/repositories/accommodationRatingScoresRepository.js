const client = require('../redisClient')

module.exports = {
    upsert: (accommodationRatingScores) => {
        return client.set(
            `accommodation_scores_${accommodationRatingScores.accommodationId}`,
            JSON.stringify(accommodationRatingScores)
        )
    },
    get: async (accommodationId) => {
        return JSON.parse(
            await client.get(
                `accommodation_scores_${accommodationId}`
            )
        )
    }
}
