const redis = require('redis');
const client = redis.createClient({
  host: process.env.REDIS_HOST,
  retry_strategy: function (options) {
    if (options.error && options.error.code === 'ECONNREFUSED') {
      // End reconnecting on a specific error and flush all commands with
      // a individual error
      return new Error('The server refused the connection');
    }
    if (options.total_retry_time > 1000 * 60 * 60) {
      // End reconnecting after a specific timeout and flush all commands
      // with a individual error
      return new Error('Retry time exhausted');
    }
    if (options.attempt > 10) {
      // End reconnecting with built in error
      return undefined;
    }
    // reconnect after
    return Math.min(options.attempt * 100, 3000);
  },
})

const { promisify } = require('util');
const hgetall = promisify(client.hgetall).bind(client);
const hset = promisify(client.hset).bind(client);
const set = promisify(client.set).bind(client);
const get = promisify(client.get).bind(client);

module.exports = {
  hgetall,
  hset,
  set,
  get
}