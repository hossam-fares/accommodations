(async () => {
    require('dotenv').config()
    const messageQueue = require('../infrastructure/messageQueue')
    const commandBus = require('../application/commands/commandBus')
    const UpsertAccommodationRatingsCommand = require('../application/commands/upsertAccommodationRatingsCommand')
    const AccommodationRatings = require('../application/domain/accommodationRatings')
    const sleep = require('util').promisify(setTimeout)

    while (true) {
        const message = await messageQueue.receiveOnce()
        const isEmpty = Object.keys(message).length === 0

        if (!isEmpty) {
            console.log('Message received' + message.id)
            const data = JSON.parse(message.message)
            if (data.name === 'reviewUpserted') {
                const review = data.review
                await commandBus.dispatch(
                    new UpsertAccommodationRatingsCommand(
                        new AccommodationRatings(
                            review.id,
                            review.parents[0].id,
                            review.travelDate,
                            review.ratings.general.general,
                            review.ratings.aspects
                        )
                    )
                )
            }

            const response = await messageQueue.acknowledgeMessage(message.id)
            console.log('Message consumed' + message.id)
        }
        await sleep(5)
    }
})().catch(e => {
    console.log(e)
})
