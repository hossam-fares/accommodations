const express = require('express')
const queryBus = require('../application/queries/queryBus')
const GetAccommodationRatingScoresQuery = require('../application/queries/getAccommodationRatingScoresQuery')

const app = express()
const port = process.env.WEB_PORT

app.get('/accommodations/:id/general-score', (req, res) => {
    const getAccommodationRatingScoresQuery = new GetAccommodationRatingScoresQuery(req.params['id']);
    queryBus.dispatch(getAccommodationRatingScoresQuery)
        .then(accommodationRatingScores => {
            res.send({ generalRatingScore: accommodationRatingScores.generalRatingScore })
        })
        .catch(e => {
            res.statusCode = 500
            res.send(e)
            console.log(e)
        })
})

app.get('/accommodations/:id/aspect-scores', (req, res) => {
    const getAccommodationRatingScoresQuery = new GetAccommodationRatingScoresQuery(req.params['id']);
    queryBus.dispatch(getAccommodationRatingScoresQuery)
        .then(accommodationRatingScores => {
            res.send({ aspectRatingScores: accommodationRatingScores.aspectRatingScores })
        })
        .catch(e => {
            res.statusCode = 500
            res.send(e)
            console.log(e)
        })
})

app.listen(port, () => {
    console.log(`Accomodation scores app started at http://localhost:${port}`)
})
