const express = require('express')
const bodyParser = require("body-parser")
const queryBus = require('../application/queries/queryBus')
const commandBus = require('../application/commands/commandBus')

const Accomodation = require('../application/domain/accommodation')
const Review = require('../application/domain/review')

const GetAllAccommodationsQuery = require('../application/queries/getAllAccommodationsQuery')
const GetAccommodationByIdQuery = require('../application/queries/getAccommodationByIdQuery')
const GetAllAccommodationReviewsQuery = require('../application/queries/getAllAccommodationReviewsQuery')
const GetAccommodationReviewByIdQuery = require('../application/queries/getAccommodationReviewByIdQuery')
const UpsertAccommodationCommand = require('../application/commands/upsertAccommodationCommand')
const UpsertReviewCommand = require('../application/commands/upsertReviewCommand')

const app = express()
app.use(bodyParser.json({ limit: '50mb' }))
const port = process.env.WEB_PORT

app.get('/accommodations', (req, res) => {
    const getAllAccomodationsQuery = new GetAllAccommodationsQuery();
    queryBus.dispatch(getAllAccomodationsQuery)
        .then(data => {
            res.send(data)
        })
        .catch(e => {
            res.statusCode = 500
            res.send(e)
            console.log(e)
        })
})

app.get('/accommodations/:id', (req, res) => {
    const getAccomodationByIdQuery = new GetAccommodationByIdQuery(req.params['id']);
    queryBus.dispatch(getAccomodationByIdQuery)
        .then(data => {
            res.send(data)
        })
        .catch(e => {
            res.statusCode = 500
            res.send(e)
            console.log(e)
        })
})

app.get('/accommodations/:accId/reviews', (req, res) => {
    const getAllAccommodationReviewsQuery = new GetAllAccommodationReviewsQuery(req.params['accId']);
    queryBus.dispatch(getAllAccommodationReviewsQuery)
        .then(data => {
            res.send(data)
        })
        .catch(e => {
            res.statusCode = 500
            res.send(e)
            console.log(e)
        })
})

app.get('/accommodations/:accId/reviews/:revId', (req, res) => {
    const getAccommodationReviewByIdQuery = new GetAccommodationReviewByIdQuery(
        req.params['accId'],
        req.params['revId']
    );
    queryBus.dispatch(getAccommodationReviewByIdQuery)
        .then(data => {
            res.send(data)
        })
        .catch(e => {
            res.statusCode = 500
            res.send(e)
            console.log(e)
        })
})

app.patch('/accommodations', (req, res) => {
    const promises = [];
    for (const accommodation of req.body) {
        const accommodationDomain = new Accomodation(
            accommodation.id,
            accommodation.vrwId,
            accommodation.names,
            accommodation.type,
            accommodation.media,
            accommodation.geo,
            accommodation.stars,
            accommodation.address,
            accommodation.contactInformation,
            accommodation.awards,
            accommodation.facilities,
            accommodation.heliosUrl,
            accommodation.heliosHistoricalUrls,
            accommodation.bookingComBlockEnabled,
            accommodation.premiumAdlinkEnabled,
            accommodation.owner,
            accommodation.status,
            accommodation.parents,
            accommodation.heliosId,
            accommodation.updatedDate,
            accommodation.popularityScore
        )

        promises.push(commandBus.dispatch(new UpsertAccommodationCommand(accommodationDomain)))
    }

    Promise.all(promises)
        .then(() => {
            res.send({ 'message': 'ok' })
        })
        .catch(e => {
            res.statusCode = 500
            res.send(e)
            console.log(e)
        })
})

app.patch('/reviews', (req, res) => {
    const promises = [];
    for (const review of req.body) {
        const reviewDomain = new Review(
            review.id,
            review.user,
            review.titles,
            review.texts,
            review.travelDate,
            review.locale,
            review.ratings,
            review.traveledWith,
            review.cooperation,
            review.entryDate,
            review.originalUserName,
            review.originalUserEmail,
            review.originalUserIp,
            review.status,
            review.parents,
            review.heliosId,
            review.updatedDate
        )
        promises.push(commandBus.dispatch(new UpsertReviewCommand(reviewDomain)))
    }

    Promise.all(promises)
        .then(() => {
            res.send({ 'message': 'ok' })
        })
        .catch(e => {
            res.statusCode = 500
            res.send(e)
            console.log(e)
        })

})

app.listen(port, () => {
    console.log(`Accomodation app started at http://localhost:${port}`)
})
