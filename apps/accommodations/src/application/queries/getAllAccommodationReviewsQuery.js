class GetAllAccommodationReviewsQuery {
    constructor(accomodationId) {
        this.name = 'getAllAccommodationReviews'
        this.accomodationId = accomodationId
    }
}

module.exports = GetAllAccommodationReviewsQuery
