class GetAccommodationReviewByIdQuery {
    constructor(accomodationId, reviewId) {
        this.name = 'getAccommodationReviewById'
        this.accomodationId = accomodationId
        this.reviewId = reviewId
    }
}

module.exports = GetAccommodationReviewByIdQuery
