const queryHandllerMapping = {
    'getAllAccommodations': require("../handlers/getAllAccommodationsHandler"),
    'getAccommodationById': require("../handlers/getAccommodationByIdHandler"),
    'getAccommodationReviewById': require("../handlers/getAccommodationReviewByIdQueryHandler"),
    'getAllAccommodationReviews': require("../handlers/getAllAccommodationReviewsQueryHandler")
}

module.exports = {
    dispatch: (query) => {
        const handler = queryHandllerMapping[query.name];

        if (!handler) {
            return Promise.reject({ 'message': 'QueryBus unable to handle ' + query.name });
        }

        return handler.handel(query)
    }
}
