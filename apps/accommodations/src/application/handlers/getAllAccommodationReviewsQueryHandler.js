const reviewRepo = require('../../persistence/repositories/reviewRepository')

async function handel(getAllAccommodationReviewsQuery) {
    return reviewRepo.getAllAccommodationReviews(getAllAccommodationReviewsQuery.accomodationId)
}

module.exports = { handel }
