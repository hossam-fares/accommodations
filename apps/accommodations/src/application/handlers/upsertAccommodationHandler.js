const accommodationRepo = require('../../persistence/repositories/accommodationRepository')
const eventBus = require('../events/eventBus')
const AccommodationUpserted = require('../events/accommodationUpsertedEvent')

async function handel(upsertAccommodationCommand) {
    const accommodation = upsertAccommodationCommand.accommodation

    await accommodationRepo.upsertAccommodation(accommodation)
    await eventBus.dispatch(new AccommodationUpserted(accommodation))
}

module.exports = { handel }
