const reviewRepo = require('../../persistence/repositories/reviewRepository')

async function handel(getAccommodationReviewByIdQuery) {
    return reviewRepo.getAccommodationReviewByid(
        getAccommodationReviewByIdQuery.accomodationId,
        getAccommodationReviewByIdQuery.reviewId
    )
}

module.exports = { handel }
