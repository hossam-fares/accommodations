const accommodationRepo = require('../../persistence/repositories/accommodationRepository')

async function handel(getAccommodationByIdQuery) {
    return accommodationRepo.getAccommodationByid(getAccommodationByIdQuery.id)
}

module.exports = { handel }
