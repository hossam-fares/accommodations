const accommodationRepo = require('../../persistence/repositories/accommodationRepository')

async function handel(getAllAccommodationsQuery) {
    return accommodationRepo.getAllAccommodations()
}

module.exports = { handel }
