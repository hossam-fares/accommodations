const reviewRepo = require('../../persistence/repositories/reviewRepository')
const eventBus = require('../events/eventBus')
const ReviewUpserted = require('../events/reviewUpsertedEvent')

async function handel(upsertReviewCommand) {
    const review = upsertReviewCommand.review

    await reviewRepo.upsertReview(review)
    await eventBus.dispatch(new ReviewUpserted(review))
}

module.exports = { handel }
