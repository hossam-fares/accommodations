const messageQueue = require('../../infrastructure/messageQueue')

module.exports = {
    dispatch: (event) => {
        return messageQueue.publish(event)
    }
}
