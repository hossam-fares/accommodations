class AccommodationUpsertedEvent {
    constructor(accommodation) {
        this.name = 'accommodationUpserted'
        this.accommodation = accommodation
    }
}

module.exports = AccommodationUpsertedEvent
