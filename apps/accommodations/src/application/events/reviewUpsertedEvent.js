class ReviewUpsertedEvent {
    constructor(review) {
        this.name = 'reviewUpserted'
        this.review = review
    }
}

module.exports = ReviewUpsertedEvent
