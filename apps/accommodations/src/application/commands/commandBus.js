const commandHandllerMapping = {
    'upsertAccommodation': require("../handlers/upsertAccommodationHandler"),
    'upsertReview': require("../handlers/upsertReviewHandler")
}

module.exports = {
    dispatch: async (command) => {
        const handler = commandHandllerMapping[command.name];

        if (!handler) {
            return Promise.reject({ 'message': 'CommandBus unable to handle ' + command.name });
        }

        await handler.handel(command)
    }
}
