class UpsertReviewCommand {
    constructor(review) {
        this.name = 'upsertReview'
        this.review = review
    }
}

module.exports = UpsertReviewCommand
