
class Accomodation {
    constructor(
        id,
        vrwId,
        names,
        type,
        media,
        geo,
        stars,
        address,
        contactInformation,
        awards,
        facilities,
        heliosUrl,
        heliosHistoricalUrls,
        bookingComBlockEnabled,
        premiumAdlinkEnabled,
        owner,
        status,
        parents,
        heliosId,
        updatedDate,
        popularityScore
    ) {
        this.id = id
        this.vrwId = vrwId
        this.names = names
        this.type = type
        this.media = media
        this.geo = geo
        this.stars = stars
        this.address = address
        this.contactInformation = contactInformation
        this.awards = awards
        this.facilities = facilities
        this.heliosUrl = heliosUrl
        this.heliosHistoricalUrls = heliosHistoricalUrls
        this.bookingComBlockEnabled = bookingComBlockEnabled
        this.premiumAdlinkEnabled = premiumAdlinkEnabled
        this.owner = owner
        this.status = status
        this.parents = parents
        this.heliosId = heliosId
        this.updatedDate = updatedDate
        this.popularityScore = popularityScore
    }
}

module.exports = Accomodation
