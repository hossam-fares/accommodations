
class Review {
    constructor(
        id,
        user,
        titles,
        texts,
        travelDate,
        locale,
        ratings,
        traveledWith,
        cooperation,
        entryDate,
        originalUserName,
        originalUserEmail,
        originalUserIp,
        status,
        parents,
        heliosId,
        updatedDate
    ) {
        this.id = id
        this.user = user
        this.titles = titles
        this.texts = texts
        this.travelDate = travelDate
        this.locale = locale
        this.ratings = ratings
        this.traveledWith = traveledWith
        this.cooperation = cooperation
        this.entryDate = entryDate
        this.originalUserName = originalUserName
        this.originalUserEmail = originalUserEmail
        this.originalUserIp = originalUserIp
        this.status = status
        this.parents = parents
        this.heliosId = heliosId
        this.updatedDate = updatedDate
    }
}

module.exports = Review