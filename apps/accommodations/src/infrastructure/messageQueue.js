const util = require('util')
const RedisSMQ = require('rsmq')
const rsmq = new RedisSMQ({ host: process.env.QUEUE_HOST, port: 6379, ns: 'rsmq' })

const createQueue = util.promisify(rsmq.createQueue)
const listQueues = util.promisify(rsmq.listQueues)
const sendMessage = util.promisify(rsmq.sendMessage)
const qname = 'accommodation_events'

async function publish(event) {
    const queues = await listQueues()

    if (!queues.includes(qname)) {
        await createQueue({ qname})
    }

    await sendMessage({qname, message: JSON.stringify(event)})
}

module.exports = {
    publish
}
