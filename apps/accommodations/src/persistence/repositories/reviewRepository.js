const pgPool = require('../pgPool')
const Review = require('../../application/domain/review')

async function getAllAccommodationReviews(accommodationId) {
    return await query('SELECT * FROM "review" WHERE CAST("parents" AS text) LIKE $1', [`%${accommodationId}%`])
}

async function getAccommodationReviewByid(accommodationId, reviewId) {
    const reviews = await query(
        'SELECT * FROM "review" WHERE CAST("parents" AS text) LIKE $1 AND id = $2',
        [
            `%${accommodationId}%`,
            reviewId
        ]
    )

    if (!reviews.length) {
        throw { message: "Unable to find review with id " + reviewId + " for accomodation with Id " + accommodationId }
    }

    return reviews[0];
}

async function query(queryStatment, queryParams) {
    const client = await pgPool.connect()
    const result = await client.query(queryStatment, queryParams)

    client.release()

    return result.rows.map(row => {
        return transformRawToDomain(row)
    });
}

async function upsertReview(review) {
    await query('DELETE FROM "review" WHERE id = $1;', [review.id])
    return query(
        'INSERT INTO "review" ("id", "userdata", "titles", "texts", "traveldate", "locale", "ratings", "traveledwith", "cooperation", "entrydate", "originalusername", "originaluseremail", "originaluserip", "status", "parents", "heliosid", "updateddate") VALUES($1, $2, $3, $4, to_timestamp($5), $6, $7, $8, $9, to_timestamp($10), $11, $12, $13, $14, $15, $16, to_timestamp($17))',
        [
            review.id,
            JSON.stringify(review.user),
            JSON.stringify(review.titles),
            JSON.stringify(review.texts),
            review.travelDate,
            review.locale,
            JSON.stringify(review.ratings),
            review.traveledWith,
            JSON.stringify(review.cooperation),
            review.entryDate,
            review.originalUserName,
            review.originalUserEmail,
            review.originalUserIp,
            JSON.stringify(review.status),
            JSON.stringify(review.parents),
            review.heliosId,
            review.updatedDate
        ]
    )
}

function transformRawToDomain(row) {
    return new Review(
        row.id,
        row.userdata,
        row.titles,
        row.texts,
        row.traveldate,
        row.locale,
        row.ratings,
        row.traveledwith,
        row.cooperation,
        row.entrydate,
        row.originalusername,
        row.originaluseremail,
        row.originaluserip,
        row.status,
        row.parents,
        row.heliosid,
        row.updateddate
    )
}

module.exports = {
    getAllAccommodationReviews,
    getAccommodationReviewByid,
    upsertReview
}
