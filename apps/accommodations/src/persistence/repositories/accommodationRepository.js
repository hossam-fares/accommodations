const pgPool = require('../pgPool')
const Accommodation = require('../../application/domain/accommodation')

async function getAllAccommodations() {
    return await query('SELECT * FROM accommodation')
}

async function getAccommodationByid(id) {
    const accommodations = await query('SELECT * FROM accommodation WHERE id = $1 LIMIT 1', [id])

    if (!accommodations.length) {
        throw { message: "Unable to find accomodation with Id " + id }
    }
    return accommodations[0];
}

async function query(queryStatment, queryParams) {
    const client = await pgPool.connect()
    const result = await client.query(queryStatment, queryParams)

    client.release()

    return result.rows.map(row => {
        return transformRawToDomain(row)
    });
}

async function upsertAccommodation(accommodation) {
    await query('DELETE FROM "accommodation" WHERE id = $1;', [accommodation.id])
    return query(
        'INSERT INTO "accommodation" ("id", "vrwid", "names", "type", "media", "geo", "stars", "addres", "contactinformation", "awards", "facilities", "heliosurl", "helioshistoricalurls", "bookingcomblockenabled", "premiumadlinkenabled", "owner", "status", "parents", "heliosid", "updateddate", "popularityscore") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, to_timestamp($20), $21);',
        [
            accommodation.id,
            accommodation.vrwId,
            accommodation.names,
            accommodation.type,
            JSON.stringify(accommodation.media),
            JSON.stringify(accommodation.geo),
            accommodation.stars,
            JSON.stringify(accommodation.address),
            JSON.stringify(accommodation.contactInformation),
            JSON.stringify(accommodation.awards),
            JSON.stringify(accommodation.facilities),
            accommodation.heliosUrl,
            JSON.stringify(accommodation.heliosHistoricalUrls),
            accommodation.bookingComBlockEnabled,
            accommodation.premiumAdlinkEnabled,
            JSON.stringify(accommodation.owner),
            JSON.stringify(accommodation.status),
            JSON.stringify(accommodation.parents),
            accommodation.heliosId,
            accommodation.updatedDate,
            accommodation.popularityScore
        ]
    )
}

function transformRawToDomain(row) {
    return new Accommodation(
        row.id,
        row.vrwid,
        row.names,
        row.type,
        row.media,
        row.geo,
        row.stars,
        row.address,
        row.contactinformation,
        row.awards,
        row.facilities,
        row.heliosurl,
        row.helioshistoricalurls,
        row.bookingcomblockenabled,
        row.premiumadlinkenabled,
        row.owner,
        row.status,
        row.parents,
        row.heliosId,
        row.updateddate,
        row.popularityscore
    )
}

module.exports = {
    getAllAccommodations,
    getAccommodationByid,
    upsertAccommodation
}
