.DEFAULT_GOAL := help

.PHONY: setup up stop help

setup: dependencies ## initialize apps
start: up ## Start apps
stop: down ## Stop apps
restart: down up ## Restart apps

up:
	docker-compose up -d

down:
	docker-compose down  --remove-orphans

dependencies:
	docker-compose run accommodations.web npm install --quiet
	docker-compose run scores.web npm install --quiet

qa:
	docker-compose run accommodations.web npm run qa
	docker-compose run scores.web npm run qa

# Based on https://suva.sh/posts/well-documented-makefiles/
help:  ## Display this help
	@awk 'BEGIN {FS = ":.*?## "; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)
